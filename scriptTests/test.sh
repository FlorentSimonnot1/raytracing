#!/bin/sh
mkdir ../output/level1
for file in ../tests/*; do
    echo "File $(basename "$file" .txt)"
    echo "Run.."
    ./../bin/lray -n 1 -i $file -o ../output/level1/$(basename "$file" .txt).ppm -d >> log.txt
    echo "Find result in ../output/level1/$(basename "$file" .txt).ppm"
done