#!/bin/sh
mkdir ../output/level3
mkdir ../output/level3/ppm6
for file in ../tests/*; do
    echo "File $(basename "$file" .txt)"
    echo "Run.."
    ./../bin/lray -n 3 -i $file -o ../output/level3/ppm6/$(basename "$file" .txt).ppm -ps 6 -d >> log.txt
    echo "Find result in ../output/level3/ppm6/$(basename "$file" .txt).ppm"
done