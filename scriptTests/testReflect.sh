#!/bin/sh
mkdir ../output/reflect
for file in ../testsReflexion/*; do
    echo "File $(basename "$file" .txt)"
    echo "Run.."
    ./../bin/lray -n 3 -i $file -o ../output/reflect/$(basename "$file" .txt).ppm -ps 6 -d >> log.txt
    echo "Find result in ../output/reflect/$(basename "$file" .txt).ppm"
done