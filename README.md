# RAYTRACER 

## Installation 

Please to use our application install *SDL2 library*. 

	sudo apt-get update -y
	sudo apt-get install libsdl2-ttf-dev
	sudo apt-get install -y libsdl2-dev

Or execute our shell script : 

	sh ./build/install.sh

This command download library, compile the project and create an executable in **bin** folder. 

## Execution

To run our application you have to execute this commande : 

	./bin/lray -n level -i fileInputPath [-o fileOutputPath] [-ps rays]

**level** is the level you want to run.
**fileInputPath** is the file path of your data file. 

<u>Optional</u>

**fileOutputPath** is the file path of the image (ppm format) which is the result of our application.
**rays** is the number of throw rays.

There are 3 levels. 

### Level 1 

This is a simple level. This application create an image from your data file and save it in your output path. Lights, shadow, mirror and transparency properties are ignored. 

	./bin/lray -n 1 -i tests/sphere.txt -o output/sphere.ppm 

### Level 2

This is a graphic level. This application create an image and render it in a graphic window. You can move camera. Lights, mirror and transparency properties are rendered too. 

	./bin/lray -n 2 -i tests/sphere.txt

### Level 3

This is an advanced level. Shadows are rendered. You have to precise the number of rays we want to throws. 

	./bin/lray -n 3 -i tests/sphere.txt -o output/sphere.ppm -ps 6

## Tests 

We have created a script to run this application with some basic geometric elements. 
You can run it with the command : 

	sh tests.sh
	sh testLevel3Rays6.sh

You can found more test ! 

## InputFiles format

*Note :* 
- [property] is an **optional property**.
- color, diffuseColor, ambientColor, specularColor are a simplification for **redComponent, greenComponent, blueComponent**.
- position, positionA, positionB, positionC, centerPosition, normal are a simplification for **X, Y, Z** coordinates.

| Object | Format description | Comments |
|--------|-------------------|-----------|
| Image | Image (width, height) | |
| Camera | |
| Light | Light (position, color, intensity, [ambientColor]) | AmbientColor is (0.2, 0.2, 0.2) by default |
| Material | Material(name, reflectionCoefficient, refractionCoefficient, diffuseColor, specularColor, specularExponent) | ReflectionCoefficient is used if you want to create a mirror object. RefractionCoefficient is used for a transparent object. If reflectionCoefficient is different to 0, refraction will be ignore. |
| Sphere | Sphere (position, radius, color, [material]) | |
| Cone | Cone(centerPosition, radius, height, [material]) | |
| Cube | Cube(centerPosition, widthPosition, heightPosition, depthPosition, color, [material]) | |
| Cylinder | Cylinder(centerPosition, radius, height, color, [material]) | |
| Plane | Plane(position, normal, color, [material]) | |
| Triangle | Triangle(positionA, positionB, positionC, color, [material]) | A, B and C position are the apex of the triangle. |

## Camera movements

If you are using graphic level, you can move the camera. 

| Movement Type | Command | Description |
|-----------------|----------|---------|
|Position| <center>↥</center> <br> <center>↦</center> <br> <center>↧</center> <br> <center>↤</center> | <center>Moves the camera up</center> <br> <center>Moves the camera right</center> <br> <center>Moves the camera down</center> <br> <center>Moves the camera left</center>  |
|    LookAt   |       <center>I</center> <br> <center>L</center> <br> <center>K</center> <br> <center>J</center> | <center>Moves the camera lookAt up</center> <br> <center>Moves the camera lookAt right</center> <br> <center>Moves the camera lookAt down</center> <br> <center>Moves the camera lookAt left</center>   |
| LookUp | <center>A</center> <br> <center>S</center> <br> <center>W</center> | <center>Moves the camera lookUp up</center> <br> <center>Moves the camera lookUp right</center> <br> <center>Moves the camera lookUp forward</center> |
| Zoom | <center>P</center> <br> <center>M</center> | <center>Moves the camera to zoom forward</center> <br> <center>Moves the camera to zoom backward</center> |

## Some examples 

You can found data examples in tests and testsReflexion folders. 