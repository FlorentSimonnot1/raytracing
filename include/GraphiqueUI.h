/**
 * @file GraphiqueUI.h
 * @author Florent Simonnot & Jean-Baptiste Pilorget
 * @brief Graphique interface
 * @version 1.0
 * @date 2020-05-28
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef RAYTRACER_GUI_H
#define RAYTRACER_GUI_H

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class Vector;
class Image;

/**
 * @class GraphiqueUI
 * 
 * @brief Manage a graphique interface to render the raytracing.
 *
**/

class GraphiqueUI
{
public:
    /**
     * @brief Construct a new Graphique interface with a specific size
     * 
     * @param w - Window width
     * @param h - Window height
     */
    GraphiqueUI(int w, int h);
    /**
     * @brief Construct a new Graphique interface from another graphique interface
     * 
     * @param g - The previous graphique interface
     */
    GraphiqueUI(const GraphiqueUI &g);

    /**
     * @brief Destroy the Graphique interface
     * 
     */
    ~GraphiqueUI();

    /**
     * @brief Create a copy of graphique interface g
     * 
     * @param g - the graphique interface we want to copy
     * @return GraphiqueUI& the new grapjique interface
     */
    GraphiqueUI &operator=(const GraphiqueUI &g);

    /**
     * @brief     Render the window with the given Image parameter by converting it
     *            to an integer array
     * 
     * @param img - A pointer to the image
     */
    void render(const Image *img);
    /**
     * @brief Set the pixel at coordinates(x, y) to the given Vector parameter color
     * 
     * @param x - Width coordinate
     * @param y - Height coordinate
     * @param color - The pixel color
     */
    //void setPixel(int x, int y, const Vector &color);

private:
    SDL_Window *myWindow;     ///< the SDL window
    SDL_Renderer *myRenderer; ///< the SDL renderer
    SDL_Texture *myTexture;   ///< the SDL texture
    int width;                ///< the window width
    int height;               ///< the windo height
};

#endif //RAYTRACER_GUI_H
