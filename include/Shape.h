/**
 * @file Shape.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Shape
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_SHAPE_H
#define RAYTRACER_SHAPE_H

#include "Vector.h"
#include "Ray.h"
#include "Material.h"
#include "Intersection.h"
#include "ShapeTypes.h"

class Vector;
class Material;
class Ray;
class Intersection;

/**
 * @class Shape
 *
 * @brief Manage a Shape.
 *
**/
class Shape
{

private:
    friend std::ostream &operator<<(std::ostream &os, const Shape &shape)
    {
        string text = shape;
        os << text;
        return os;
    };

protected:
    Vector color;
    string materialName;

public:
    /**
     * @brief Construct a new Shape with given parameters
     */
    Shape() : materialName("default"), color(Vector()){};

    /**
     * @brief Default Shape constructor
     */
    ~Shape(){};

    /**
     * @brief Get the Shape position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition() = 0;

    /**
     * @brief Get the Shape position
     *
     * @return The Vector corresponding to the position
     */
    virtual const Vector getColor() const;

    /**
     * @brief Get the Shape material name
     *
     * @return The Vector corresponding to the material name
     */
    virtual const string getMaterialName() const;

    /**
     * @brief Check if the ray intersect the Shape and save it in the intersection parameter if true
     *
     * @param ray - the ray
     * @param intersection - the intersection
     * @return true if the ray intersect the Shape, else return false
     */
    virtual bool intersect(const Ray &ray, Intersection &intersection) = 0;


    virtual ShapeTypes getShape() = 0;
    /**
     * @brief Construct the Shape information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif
