/**
 * @file ShapeTypes.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief ShapeTypes
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_SHAPETYPES_H
#define RAYTRACER_SHAPETYPES_H

#include <iostream>
#include <string>

using namespace std;

/**
 * @class ShapeTypes
 *
 * @brief Enumerate all shapes types used
 *
**/
enum ShapeTypes
{
    IMAGE,
    CAMERA,
    LIGHT,
    SPHERE,
    CUBE,
    CONE,
    CYLINDER,
    PLANE,
    TRIANGLE,
    MATERIAL,
    NONE
};

/**
 * @brief Create a new ShapeTypes by parsing the string shape parameter
 *
 * @param shape - The description to be parsed
 * @return The ShapeTypes created by parsing.
 */
ShapeTypes parseShape(string shape);

#endif //RAYTRACER_SHAPETYPES_H
