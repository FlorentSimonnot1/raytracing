/**
 * @file Intersection.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Intersection
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_INTERSECTION_H
#define RAYTRACER_INTERSECTION_H

#include "Vector.h"
#include "Shape.h"

class Vector;
class Shape;

/**
 * @class Intersection
 *
 * @brief Manage an Intersection.
 *
**/
class Intersection
{
private:
public:
    Vector pos;
    Vector normal;
    double t;
    Shape *shape;

    /**
     * @brief Construct a new Intersection with specific parameters
     *
     * @param position - the position of the intersection point
     * @param n - the normal of the intersection point
     * @param tValue the distance value between the ray and the intersection point
     * @param s - the shape involved in the intersection
     */
    Intersection(Vector position = Vector(), Vector n = Vector(), double tValue = 0., Shape *s = nullptr) : pos(position), normal(n), t(tValue), shape(s){};

    /**
     * @brief Default Cone constructor
     */
    ~Intersection();

    /**
     * @brief Initialize an Intersection with the same properties as the parameter
     *
     * @param i - an Intersection
     * @return the new Intersection created
     */
    Intersection &operator=(const Intersection &i);
};

#endif
