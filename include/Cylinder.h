/**
 * @file Cylinder.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Cylinder 
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RATRACER_CYLINDER_H
#define RAYTRACER_CYLINDER_H

#include <iostream>
#include <string>
#include "Vector.h"
#include "Shape.h"
#include "Intersection.h"
#include "Ray.h"
using namespace std;

class Vector;
class Intersection;
class Material;
class Ray;

/**
 * @class Cylinder
 *
 * @brief Manage a Cylinder Shape.
 *
**/
class Cylinder : public Shape
{
public:
    Vector basePoint;
    double height;
    double radius;

    /**
     * @brief Construct a new Cylinder with specific parameters
     *
     * @param bp - the center position of the cylinder base
     * @param h - the cylinder height
     * @param r - the cylinder radius
     * @param c - the cylinder color
     * @param materialName - the cylinder material name
     */
    Cylinder(const Vector &bp, double h, double r, Vector c, string materialName = "default") : basePoint(bp), height(h), radius(r), topPoint(Vector(bp[0], bp[1] + height, bp[2]))
    {
        this->color = c;
        this->materialName = materialName;
    };

    /**
     * @brief Default Cylinder constructor
     */
    ~Cylinder(){};

    /**
     * @brief Create a new Cylinder by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Cylinder created by parsing.
     */
    static Cylinder *create(string description);

    /**
     * @brief Get the Cylinder position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition();

    virtual ShapeTypes getShape();


    /**
     * @brief Check if the ray intersect the Cylinder and save it in the intersection parameter if true
     *
     * @param ray - the ray
     * @param intersection - the intersection
     * @return true if the ray intersect the Cylinder, else return false
     */
    virtual bool intersect(const Ray &ray, Intersection &intersection);

private:
    Vector topPoint;
    bool baseIntersect(const Ray &ray, const Vector &c, Intersection &intersection);
    Vector normalIn(const Vector &point);
};

#endif
