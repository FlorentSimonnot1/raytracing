/**
 * @file Material.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Material
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_MATERIAL_H
#define RAYTRACER_MATERIAL_H

#include "Vector.h"
#include <iostream>
using namespace std;

class Vector;

/**
 * @class Material
 *
 * @brief Manage a Material.
 *
**/
class Material
{
public:
    struct Specular
    {
        Vector color;
        double exponent;
        Specular() : Specular(Vector(255, 255, 255), 5){};
        Specular(Vector color, double exponent) : color(color), exponent(exponent) {}
    };

    struct Diffuse
    {
        Vector color;
        double coefficient;
        Diffuse() : Diffuse(Vector(1, 1, 1), 5){};
        Diffuse(Vector color, double coefficient) : color(color), coefficient(coefficient) {}
    };

    /**
     * @brief Construct a new Material with default parameters
     */
    Material() : Material("default"){};

    /**
     * @brief Construct a new Material with given name
     *
     * @param name
     */
    Material(string name) : Material(name, 0., 0., Specular(), Diffuse()){};

    /**
     * @brief Construct a new Material with specific parameters
     *
     * @param name - material name
     * @param reflectionCoef - material reflection coefficient
     * @param refractionIndex - material refraction index
     */
    Material(string name, double reflectionCoef, double refractionIndex, Material::Specular specular, Material::Diffuse diffuse) : name(name), reflectionCoef(reflectionCoef), refractionIndex(refractionIndex), specular(specular), diffuse(diffuse) {}

    /**
     * @brief Default Material constructor
     */
    ~Material(){};

    /**
     * @brief Create a new Material by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Material created by parsing.
     */
    static Material create(string description);

    /**
     * @brief Get the current reflection coefficient
     *
     * @return the reflection coefficient
     */
    double getReflectionCoef() { return reflectionCoef; };

    /**
     * @brief Get the current refraction coefficient
     *
     * @return the refraction coefficient
     */
    double getRefractionCoef() { return refractionIndex; };

    // TODO
    /**
     * @brief Get the current specular properties
     *
     * @return the specular
     */
    Specular getSpecularProperties() { return specular; }

    // TODO
    /**
     * @brief Get the current diffuse properties
     *
     * @return the diffuse
     */
    Diffuse getDiffuseProperties() { return diffuse; }

    // TODO
    /**
     * @brief Get the current material name
     *
     * @return the material name
     */
    string getName()
    {
        return name;
    }

    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;

private:
    string name;
    double reflectionCoef;
    double refractionIndex;
    Specular specular;
    Diffuse diffuse;

    friend std::ostream &operator<<(std::ostream &, Material);
    friend std::istream &operator>>(std::istream &, Material &);
};

#endif //RAYTRACER_MATERIAL_H
