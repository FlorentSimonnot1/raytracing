/**
 * @file Image.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Image
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_IMAGE_H
#define RAYTRACER_IMAGE_H

#include <iostream>
#include <fstream>

using namespace std;

/**
 * @class Image
 *
 * @brief Manage an Image.
 *
**/
class Image
{
public:
    struct Pixel
    {
        unsigned char red, green, blue;
        Pixel() : red(0), green(0), blue(0){};
        Pixel(unsigned char r, unsigned char g, unsigned char b) : red(r), green(g), blue(b){};
    };

    /**
     * @brief Create an empty Image
     */
    Image() : width(0), height(0), pixels(nullptr){};

    /**
     * @brief Construct a new Image with specific parameters
     *
     * @param w - Image width
     * @param h - Image height
     * @param color - Image color
     */
    Image(const int &w, const int &h, const Pixel &color = Pixel(0, 0, 0)) : width(w), height(h), pixels(NULL)
    {
        pixels = new Pixel[w * h];
        for (int i = 0; i < w * h; ++i)
            pixels[i] = color;
    };

    /**
     * @brief Get the element at a specific index
     *
     * @param i - the index
     * @return the element at index i
     */
    const Pixel &operator[](int i) const { return pixels[i]; }

    /**
     * @brief Get the element at a specific index
     *
     * @param i - the index
     * @return the element at index i
     */
    Pixel &operator[](int i) { return pixels[i]; }

    /**
     * @brief Initialize an Image with the same properties as the image parameter
     *
     * @param image - an image
     * @return the new image created
     */
    Image &operator=(const Image &image);

    /**
     * @brief Get the current width
     *
     * @return the width
     */
    int getWidth()
    {
        return width;
    }

    /**
     * @brief Get the current height
     *
     * @return the height
     */
    int getHeight()
    {
        return height;
    }

    /**
     * @brief Change the pixel color at specific coordinates
     *
     * @param x - coordinate x
     * @param y - coordinate y
     * @param color - the new color
     */
    void setPixel(int x, int y, Pixel color) const
    {
        if (x * width + y > width * height)
            return;
        pixels[(height - x - 1) * width + y] = color;
    }

    /**
     * @brief Destruct the image properly
     */
    ~Image()
    {
        if (pixels != NULL)
            delete[] pixels;
    }

    /**
     * @brief Convert an Image pixels into an integer array
     *
     * @return the array
     */
    const int *toIntArray() const;

    /**
     * @brief Save the current image in ppm format
     *
     * @param filename - the name of the saved image file
     */
    void savePPM(const string &filename);

    /**
     * @brief Create a new Image by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Image created by parsing.
     */
    static Image *create(string description);

    /**
     * @brief Save the image in bmp format
     * 
     * @param filename - the name saved of the image file
     */
    void saveBMP(const char *filename);

private:
    int width, height;
    Pixel *pixels;
};

#endif //RAYTRACER_IMAGE_H
