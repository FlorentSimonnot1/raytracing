/**
 * @file Cube.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Cube
 * @version 1.0
 * @date 2020-06-06
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef RAYTRACER_CUBE_H
#define RAYTRACER_CUBE_H

#include <iostream>
#include <string>
#include "Vector.h"
#include "Shape.h"

using namespace std;

class Vector;
class Shape;

/**
 * @class Cube
 * 
 * @brief Manage a Cube Shape.
 *
**/
class Cube : public Shape
{
private:
    Vector p1, p2, p3, p4;
    friend std::ostream &operator<<(std::ostream &, Cube);

public:
    /**
     * @brief Construct a new Cube
     */
    Cube();

    /**
     * @brief Default Cube constructor
     */
    ~Cube();

    /**
     * @brief Construct a new Cube with specific parameters
     *
     * @param v1 - the top left corner position
     * @param v2 - the x edge
     * @param v3 - the y edge
     * @param v4 - the z edge
     * @param c - the cube color
     * @param materialName - the cube material name
     */
    Cube(Vector v1, Vector v2, Vector v3, Vector v4, Vector c, string materialName = "default")
        : p1(v1), p2(v2), p3(v3), p4(v4)
    {
        this->color = c;
        this->materialName = materialName;
    }

    /**
     * @brief Construct a new Cube with given parameters
     *
     * @param vectors - An array of 4 vectors
     * @param c - The color of the cube
     * @param mat - the cube material name
     */
    Cube(Vector vectors[4], Vector c, string materialName = "default") : Cube(vectors[0], vectors[1], vectors[2], vectors[3], c, materialName) {}

    /**
     * @brief Create a new Cube by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Cube created by parsing.
     */
    static Cube *create(string description);

    /**
     * @brief Calculate the smallest coordinates between the Cube's 4 vectors
     *
     * @param var - the coordinate between (x, y, z)
     * @return the smallest coordinates between the Cube's 4 vectors
     */
    double getMin(const char var);

    /**
     * @brief Calculate the biggest coordinates between the Cube's 4 vectors
     *
     * @param var - the coordinate between (x, y, z)
     * @return the biggest coordinates between the Cube's 4 vectors
     */
    double getMax(const char var);

    /**
     * @brief Get the Cube position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition();

    /**
     * @brief Check if the ray intersect the Cube and save it in the intersection parameter if true
     *
     * @param ray - the ray
     * @param intersection - the intersection
     * @return true if the ray intersect the Cube, else return false
     */
    virtual bool intersect(const Ray &ray, Intersection &intersection);

    virtual ShapeTypes getShape();

    virtual operator std::string() const;
};

#endif
