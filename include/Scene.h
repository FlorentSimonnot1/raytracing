/**
 * @file Cone.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Scene
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_SCENE_H
#define RAYTRACER_SCENE_H

#include <vector>
#include <map>
#include <string>
#include <cmath>
#include "Sphere.h"
#include "Cube.h"
#include "Vector.h"
#include "Camera.h"
#include "Image.h"
#include "Shape.h"
#include "ShapeTypes.h"
#include "Light.h"

using namespace std;

class Light;
/**
 * @brief Class which represents a scene. 
 * Contains a camera, lights and shapes.
 */
class Scene
{

private:
    Image image;
    vector<Light> lights;
    Camera camera;
    map<std::string, Material> materials;
    map<ShapeTypes, vector<Shape *>> shapes;
    int level;
    int rays;
    string input;
    string output;
    bool isDebugMode;

    friend std::ostream &operator<<(std::ostream &, Scene &);

    Vector getColor(const Ray &ray, int nbRecursion = 0);

public:
    /**
     * @brief Construct a new Scene object
     * 
     * @param lev - the level chosen by user
     * @param in - the input file
     * @param out - the output file
     * @param r - number of rays to throw
     * @param isDebugMode - flag if user wants to use the debbuger
     */
    Scene(int lev, string in, string out, int r, bool isDebugMode = false) : image(Image()), lights(vector<Light>()), materials(), camera(Camera()),
                                                                             shapes(), level(lev), rays(r), input(in), output(out), isDebugMode(isDebugMode){};
    /**
     * @brief Destroy the Scene object
     * 
     */
    ~Scene(){};

    // TODO
    /**
     * @brief Check the scene
     */
    void checkScene();
    // TODO
    /**
     * @brief Get the file extension
     *
     * @return the file extension
     */
    string getFileExtension() const { return output.substr(output.find_last_of(".") + 1); }
    // TODO
    /**
     * @brief Get the debug mode
     *
     * @return the debug mode
     */
    const bool getDebugMode() { return isDebugMode; }

    // TODO
    /**
     * @brief Check if the scene ray intersect a shadow
     *
     * @param intersection - the intersection point of the ray with a shape
     * @return true if the scene ray intersect a shadow, else return false
     */
    bool intersectShadow(Intersection &intersection) const;

    // TODO
    /**
      * @brief Check if the ray intersect the Scene and save it in the intersection parameter if true
      *
      * @param ray - the ray
      * @param intersection - the intersection
      * @return true if the ray intersect the Scene, else return false
      */
    bool intersect(const Ray &ray, Intersection &intersection) const;

    // TODO
    /**
     * @brief Search a material by its name
     *
     * @param name - the material name
     * @return the material associated to the name
     */
    Material searchMaterialByName(const string name) const;
    /**
     * @brief Add a camera in this scene
     * 
     * @param camera - a camera
     */
    void addCamera(const Camera &camera);
    /**
     * @brief Add a light in this scene
     * 
     * @param light - a light
     */
    void addLight(const Light &light);
    /**
     * @brief Add an image in this scene
     * 
     * @param image - an image
     */
    void addImage(const Image &image);

    // TODO
    /**
     * @brief Get the mirror color of the intersection
     *
     * @param color - the color
     * @param ray - the ray
     * @param intersection - the intersection
     * @param nRecursion - the number of recursion
     */
    void getMirrorColor(Vector &color, const Ray &ray, const Intersection &intersection, int &nRecursion);
    // TODO
    /**
     * @brief Get the transparency color of the intersection
     *
     * @param color - the color
     * @param ray - the ray
     * @param intersection - the intersection
     * @param nRecursion - the number of recursion
     * @param refractionCoef - the refraction coefficient
     */
    void getTransparencyColor(Vector &color, const Ray &ray, const Intersection &intersection, int &nRecursion, double refractionCoef);

    /**
     * @brief Add a material in the scene
     * 
     * @param material - the material we want to add
     */
    void addMaterial(Material material);

    /**
     * @brief Add a shape in this scene
     * 
     * @param shapeType - the shape type (Sphere, Cub etc..)
     * @param shape - the shape
     * @see ShapeTypes
     */
    void addShape(const ShapeTypes &shapeType, Shape *shape);
    /**
     * @brief Get the scene camera
     * 
     * @return Camera - the camera of the scene
     */
    Camera getCamera() const { return camera; }
    /**
     * @brief Get the scene image
     * 
     * @return Image* - the address of the scene image
     */
    Image *getImage() { return &image; }
    /**
     * @brief Get the scene level
     * 
     * @return const int - the level chosen by the user (1, 2 or 3)
     */
    const int getLevel() { return level; }
    /**
     * @brief Get the output path for save file
     * 
     * @return string - the output path 
     */
    string getOutputPath() { return output; }

    /**
     * @brief Move the camera position in horizontal or vertical direction
     * 
     * @param dir - the direction to move camera
     */
    void moveCameraPosition(Camera::Direction dir) { camera.move(dir); };
    /**
     * @brief Move the camera position in depth direction
     * 
     * @param dir - the direction to move camera
     */
    void zoomCamera(Camera::Direction dir) { camera.zoom(dir); };
    /**
     * @brief 
     * 
     * @param dir 
     */
    void moveLookAtCamera(Camera::Direction dir) { camera.moveLookAt(dir); };

    // TODO
    /**
     * @brief Set up a camera to a specific direction
     *
     * @param dir - the new direction to be set up to
     */
    void moveUpCamera(Camera::Direction dir) { camera.setUp(dir); };
    /**
     * @brief Render the scene
     * 
     */
    void generateScene();
    /**
     * @brief Save the scene in a file
     * 
     * @param fileName - the filename of the output file
     */
    void generateImage(const string &fileName);

    // TODO
    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif //RAYTRACER_SCENE_H
