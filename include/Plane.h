/**
 * @file Plane.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Plane
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */


#ifndef RAYTRACER_PLANE_H
#define RAYTRACER_PLANE_H

#include <iostream>
#include <string>
#include "Shape.h"
#include "Vector.h"

using namespace std;

class Vector;
class Shape;
class Plane : public Shape
{
private:
    Vector pos;
    Vector normal;
    friend std::ostream &operator<<(std::ostream &, const Plane &);

public:
    /**
     * @brief Construct a new Plane with specific parameters
     *
     * @param pos - The point of origin of the plan
     * @param normal - the normal of the plan
     * @param vector - the color of the plan
     * @param materialName - the plane material name
     */
    Plane(Vector pos, Vector normal, Vector vector, string materialName = "default");

    /**
     * @brief Construct a new Plane with given parameters
     */
    Plane();

    /**
     * @brief Default Plane constructor
     */
    ~Plane();
    static Plane *create(string description);

    /**
     * @brief Get the Plane position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition() { return pos; }

    /**
      * @brief Check if the ray intersect the Plane and save it in the intersection parameter if true
      *
      * @param ray - the ray
      * @param intersection - the intersection
      * @return true if the ray intersect the Plane, else return false
      */
    virtual bool intersect(const Ray &ray, Intersection &intersection);

    virtual ShapeTypes getShape();


    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif
