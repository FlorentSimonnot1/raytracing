/**
 * @file Camera.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Camera
 * @version 1.0
 * @date 2020-06-06
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef RAYTRACER_CAMERA_H
#define RAYTRACER_CAMERA_H

#include "Vector.h"
#include "Ray.h"
#include "Image.h"
#include <iostream>

class Vector;

/**
 * @class Camera
 * 
 * @brief Manage a camera to monitor the raytracing.
 *
**/
class Camera
{

private:
    Vector pos;
    Vector lowerLeftCorner;
    Vector horizontal;
    Vector vertical;
    Vector u, v, w;
    Vector lookat;
    Vector vup;
    double vfov;
    double lensRadius;
    double aspectRatio;
    double aperture;
    double dist;

    friend std::ostream &operator<<(std::ostream &, const Camera &);

public:
    enum Direction
    {
        up,
        down,
        left,
        right,
        forward,
        backward
    };

    /**
     * @brief Construct a new Camera with specific parameters
     * 
     * @param lookfrom - the camera position
     * @param lookat - the camera's looking direction
     * @param vup - the camera axis
     * @param vfov - the camera front of view
     * @param aspectRatio - the camera aspect radio
     * @param aperture - the camera lens radius
     * @param dist - the camera focus distance
     */
    Camera(Vector lookfrom, Vector lookat, Vector vup, double vfov, double aspectRatio, double aperture, double dist);

    /**
     * @brief Construct a new Camera with given parameters
     */
    Camera() : Camera(Vector(0, 0, 0), Vector(0, 0, 0), Vector(0, 0, 0), 0., 0., 0., 0.){};

    /**
     * @brief Default Camera constructor
     */
    ~Camera(){};

    /**
     * @brief Create a new Camera by parsing the string description parameter
     *
     * @param description - The description to be parsed
     *
     * @return The Camera created by parsing.
     */
    static Camera *create(string description);

    /**
     * @brief Get the camera position
     *
     * @return The Vector corresponding to the position
     */
    const Vector getPos() const;

    const Vector getLookAt() const { return lookat; }

    /**
     * @brief Create and return a new Ray with specific parameters
     *
     * @param i - the height coordinate
     * @param j - the width coordinate
     * @param im - an image
     *
     * @return the Ray created
     */
    Ray createRay(const int &i, const int &j, Image &im);

    /**
     * @brief Set the front of view value of the current camera
     *
     * @param v - the new front of view value for the camera
     *
     * @return the Camera with the new front of view value
     */
    Camera &setVfov(double v);

    /**
     * @brief Set the position
     *
     * @param x - coordinate x
     * @param y - coordinate y
     * @param z - coordinate z
     *
     * @return the Camera with the new position
     */
    Camera &setPosition(const double x, const double y, const double z);

    /**
     * @brief Set the look at
     *
     * @param x - coordinate x
     * @param y - coordinate y
     * @param z - coordinate z
     *
     * @return the Camera with the new look at
     */
    Camera &setLookAt(const double x, const double y, const double z);

    /**
     * @brief Move the Camera to a specific Direction
     *
     * @param dir - The direction to move to
     */
    void move(Camera::Direction dir);

    /**
     * @brief Zoom the Camera to a specific Direction
     *
     * @param dir - The direction to zoom to
     */
    void zoom(Camera::Direction dir);

    /**
     * @brief Move and Look at the Camera to a specific Direction
     *
     * @param dir - The direction to move and look at to
     */
    void moveLookAt(Camera::Direction dir);

    /**
     * @brief Set up the Camera to a specific Direction
     *
     * @param dir - The direction to set up to
     */
    void setUp(Camera::Direction dir);

    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif
