/**
 * @file Light.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Light
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_LIGHT_H
#define RAYTRACER_LIGHT_H

#include <vector>
#include <iostream>
#include <string>
#include "Vector.h"
#include "Scene.h"
#include "Ray.h"
#include "Intersection.h"

using namespace std;

class Vector;
class Scene;

/**
 * @class Light
 *
 * @brief Manage a Light.
 *
**/
class Light
{

private:
    Vector pos;
    double intensity;
    double ambientTerm;
    Vector color;

    friend std::ostream &operator<<(std::ostream &, Light);

public:
    /**
     * @brief Construct a new Light with given parameters
     */
    Light() : pos(Vector()), intensity(0.), ambientTerm(0.2), color(Vector(1, 1, 1)){};

    /**
     * @brief Construct a new Light with specific parameters
     *
     * @param position - the light position
     * @param i - the light intensit
     * @param ambientTerm - the scene ambient term
     * @param color - the light color
     */
    Light(Vector position, double i, double ambientTerm = 0.2, Vector color = Vector(1, 1, 1)) : pos(position), intensity(i), ambientTerm(ambientTerm), color(color){};

    /**
     * @brief Default Light constructor
     */
    ~Light(){};

    /**
     * @brief Get the Light color
     *
     * @param scene - the scene
     * @param ray - the ray
     * @param intersection - the intersection
     * @return The Vector corresponding to the color
     */
    Vector getColor(const Scene &scene, const Ray &ray, const Intersection &intersection);

    /**
     * @brief Create a new Light by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Light created by parsing.
     */
    static Light *create(string description);

    /**
     * @brief Get the current Light intensity
     *
     * @return the intensity
     */
    double getIntensity() const;

    /**
     * @brief Get the Light position
     *
     * @return The Vector corresponding to the position
     */
    Vector getPos() const;

    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif //RAYTRACER_LIGHT_H
