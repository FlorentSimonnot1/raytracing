/**
 * @file Ray.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Ray
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_RAY_H
#define RAYTRACER_RAY_H

#include "Vector.h"

/**
 * @class Ray
 *
 * @brief Manage a Ray.
 *
**/
class Ray
{

public:
    /**
     * @brief Construct a new Ray with specific parameters
     *
     * @param o - ray origin
     * @param d - ray direction
     */
    Ray(const Vector &o, const Vector &d) : origin(o), direction(d){};

    /**
     * @brief Default Ray constructor
     */
    ~Ray(){};
    Vector origin, direction;
};

#endif //RAYTRACER_RAY_H
