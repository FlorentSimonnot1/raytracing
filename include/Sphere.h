/**
 * @file Sphere.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Sphere
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_SPHERE_H
#define RAYTRACER_SPHERE_H

#include "Vector.h"
#include "Ray.h"
#include "Shape.h"
#include "Material.h"
#include <string>
#include <iostream>

using namespace std;

/**
 * @class Sphere
 *
 * @brief Manage a Sphere Shape.
 *
**/
class Sphere : public Shape
{

public:
    /**
     * @brief Construct a new Sphere with specific parameters
     *
     * @param o - sphere origin
     * @param r - sphere radius
     * @param c - sphere color
     * @param materialName - sphere material name
     */
    Sphere(const Vector &o, const double r, const Vector &c, string materialName = "default") : origin(o), radius(r)
    {
        this->color = c;
        this->materialName = materialName;
    };

    /**
     * @brief Default Sphere constructor
     */
    ~Sphere();

    Vector origin;
    double radius;

    /**
     * @brief Create a new Sphere by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Sphere created by parsing.
     */
    static Sphere *create(string description);

    /**
     * @brief Get the Sphere position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition();

    /**
     * @brief Check if the ray intersect the Sphere and save it in the intersection parameter if true
     *
     * @param ray - the ray
     * @param intersection - the intersection
     * @return true if the ray intersect the Sphere, else return false
     */
    virtual bool intersect(const Ray &ray, Intersection &intersection);

    virtual ShapeTypes getShape();


    /**
     * @brief Construct the Sphere information in string format
     * @return the string created
     */
    virtual operator std::string() const;

private:
    friend std::ostream &operator<<(std::ostream &, const Sphere &);
};

#endif //RAYTRACER_SPHERE_H
