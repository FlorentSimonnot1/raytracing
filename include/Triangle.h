/**
 * @file Triangle.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Triangle
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_TRIANGLE_H
#define RAYTRACER_TRIANGLE_H

#include "Shape.h"
#include "Vector.h"
#include "Material.h"

using namespace std;

class Vector;
class Shape;

/**
 * @class Triangle
 *
 * @brief Manage a Triangle Shape.
 *
**/
class Triangle : public Shape
{

private:
    Vector p0;
    Vector p1;
    Vector p2;
    friend std::ostream &operator<<(std::ostream &, const Triangle &);

public:
    /**
     * @brief Construct a new Triangle with specific parameters
     *
     * @param p0 - triangle first vertex
     * @param p1 - triangle second vertex
     * @param p2 - triangle third vertex
     * @param color - triangle color
     * @param materialName - triangle material name
     */
    Triangle(Vector p0, Vector p1, Vector p2, Vector color, string materialName = "default") : p0(p0), p1(p1), p2(p2)
    {
        this->color = color;
        this->materialName = materialName;
    };

    /**
     * @brief Construct a new Triangle with given parameters
     */
    Triangle() : Triangle(Vector(), Vector(), Vector(), Vector()){};

    /**
     * @brief Default Triangle constructor
     */
    ~Triangle();

    /**
     * @brief Create a new Triangle by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Triangle created by parsing.
     */
    static Triangle *create(string description);

    /**
     * @brief Get the Triangle position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition();

    virtual ShapeTypes getShape();


    /**
     * @brief Check if the ray intersect the Triangle and save it in the intersection parameter if true
     *
     * @param ray - the ray
     * @param intersection - the intersection
     * @return true if the ray intersect the Triangle, else return false
     */
    virtual bool intersect(const Ray &ray, Intersection &intersection);
};

#endif
