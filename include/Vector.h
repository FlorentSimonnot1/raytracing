/**
 * @file Vector.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Vector
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_VECTOR_H
#define RAYTRACER_VECTOR_H

#include <iostream>
#include "math.h"
#include <random>

static std::default_random_engine engine;
static std::uniform_real_distribution<double> uniform(0.1);

/**
 * @class Vector
 *
 * @brief Manage a Vector.
 *
**/
class Vector
{
public:

    /**
     * @brief Construct a new Vector with specific parameters
     *
     * @param x - vector x parameter
     * @param y - vector y parameter
     * @param z - vector z parameter
     */
    Vector(double x = 0, double y = 0, double z = 0)
    {
        coords[0] = x;
        coords[1] = y;
        coords[2] = z;
    }

    /**
     * @brief Default Vector constructor
     */
    ~Vector(){};

    /**
     * @brief Get the element at a specific index
     *
     * @param i - the index
     * @return the element at index i
     */
    const double &operator[](int i) const { return coords[i]; }

    /**
     * @brief Get the element at a specific index
     *
     * @param i - the index
     * @return the element at index i
     */
    double &operator[](int i) { return coords[i]; }

    /**
     * @brief Calculate current norms squared
     *
     * @return the calculation result
     */
    double getNorm2()
    {
        return coords[0] * coords[0] + coords[1] * coords[1] + coords[2] * coords[2];
    }

    /**
     * @brief Calculate the current normalization
     *
     * @return the calculation result
     */
    void normalize()
    {
        double norm = sqrt(getNorm2());
        coords[0] /= norm;
        coords[1] /= norm;
        coords[2] /= norm;
    }

    /**
     * @brief Get the current norm
     *
     * @return the norm vector
     */
    Vector getNormalized()
    {
        Vector result(*this);
        result.normalize();
        return result;
    }

    /**
     * @brief Calculate the increment between the current vector and a given one
     *
     * @param v - the vector
     * @return the result vector
     */
    Vector &operator+=(const Vector &v)
    {
        coords[0] += v[0];
        coords[1] += v[1];
        coords[2] += v[2];
        return *this;
    }

    /**
     * @brief Calculate the decrement between the current vector and a given one
     *
     * @param v - the vector
     * @return the result vector
     */
    Vector &operator-=(const Vector &v)
    {
        coords[0] -= v[0];
        coords[1] -= v[1];
        coords[2] -= v[2];
        return *this;
    }

    /*double operator*(const Vector &v)
    {
        return coords[0] * v[0] + coords[1] * v[1] + coords[2] * v[2];
    }*/

    Vector reflect(const Vector &vec);

    /**
     * @brief Calculate the multiplication between the current vector and a given one
     *
     * @param v - the vector
     * @return the result vector
     */
    Vector &operator*=(double d);

    /**
     * @brief Calculate the division between the current vector and a given one
     *
     * @param v - the vector
     * @return the result vector
     */
    Vector &operator/=(const double d);

    /**
     * @brief Calculate if the current vector and a given one are equal
     *
     * @param v - the vector
     * @return true if they are, else return false
     */
    bool operator==(const Vector &v) const
    {
        return coords[0] == v[0] && coords[1] == v[1] && coords[2] == v[2];
    }

    /**
     * @brief Calculate if the current vector and a given one are different
     *
     * @param v - the vector
     * @return true if they are, else return false
     */
    bool operator!=(const Vector &v) const
    {
        return !(*this == v);
    }

    /**
     * @brief Calculate the current norm
     *
     * @return the calculation result
     */
    const double norm() const;

    /**
     * @brief Calculate the inverse of a vector
     *
     * @return the vector result
     */
    const Vector getInverse() const;

    /**
     * @brief Calculate the given vector norm
     *
     * @return the calculation result
     */
    double norm(const Vector &vec);

private:
    double coords[3];
    friend std::ostream &operator<<(std::ostream &, const Vector &);
    friend std::istream &operator>>(std::istream &, Vector &);
};

/**
 * @brief Calculate the increment between two vectors
 *
 * @param a - first vector
 * @param b - second vector
 * @return new vector
 */
Vector operator+(const Vector &a, const Vector &b);

/**
 * @brief Calculate the decrement between two vectors
 *
 * @param a - first vector
 * @param b - second vector
 * @return new vector
 */
Vector operator-(const Vector &a, const Vector &b);

/**
 * @brief Calculate the decrement of a vector
 * @param a - a vector
 * @return new vector
 */
Vector operator-(const Vector &a);

/**
 * @brief Calculate the vector 'a' multiplied by double 'b' value
 *
 * @param a - vector
 * @param b - double
 * @return new vector
 */
Vector operator*(const Vector &a, double b);

/**
 * @brief Calculate the vector 'b' multiplied by double 'a' value
 *
 * @param a - double
 * @param b - vector
 * @return new vector
 */
Vector operator*(double a, const Vector &b);

/**
 * @brief Calculate the multiplication between two vectors
 *
 * @param a - first vector
 * @param b - second vector
 * @return new vector
 */
Vector operator*(const Vector &a, const Vector &b);

/**
 * @brief Calculate the vector 'a' divided by double 'b' value
 *
 * @param a - vector
 * @param b - double
 * @return new vector
 */
Vector operator/(const Vector &a, double b);

/**
 * @brief Calculate the dot value between two vectors
 *
 * @param a - first vector
 * @param b - second vector
 * @return the calculation result
 */
double dot(const Vector &a, const Vector &b);

/**
 * @brief Calculate the cross value between two vectors
 *
 * @param a - first vector
 * @param b - second vector
 * @return the calculation result
 */
Vector cross(const Vector &a, const Vector &b);

/**
 * @brief Calculate a new Vector with a random cosine
 * @param v - vector
 * @return new vector
 */
Vector randomCos(const Vector &v);
#endif //RAYTRACER_VECTOR_H
