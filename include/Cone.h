/**
 * @file Cone.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Cone
 * @version 1.0
 * @date 2020-06-06
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef RAYTRACER_CONE_H
#define RAYTRACER_CONE_H

#include "Vector.h"
#include "Ray.h"
#include "Shape.h"
#include "Material.h"
#include "Shape.h"

using namespace std;

/**
 * @class Cone
 * 
 * @brief Manage a Cone Shape.
 *
**/
class Cone : public Shape
{

private:
    Vector center;
    double radius;
    double height;
    double cosinusAlpha;
    double sinusAlpha;
    friend std::ostream &operator<<(std::ostream &, const Cone &);
    Vector normalIn(const Vector &vec);
    bool baseIntersect(const Ray &ray, const Vector &c, Intersection &intersection);

public:
    /**
     * @brief Construct a new Cone with specific parameters
     * 
     * @param c - the position of the vertex
     * @param rad - the cone radius
     * @param h - the cone height
     * @param col - the cone color
     * @param materialName - the cone material name
     */
    Cone(Vector c, double rad, double h, Vector col, string materialName = "default");

    /**
     * @brief Construct a new Cone with given parameters
     */
    Cone() : Cone(Vector(), 0, 0, Vector())
    {
        cosinusAlpha = 0;
        sinusAlpha = 0;
    };

    /**
     * @brief Default Cone constructor
     */
    ~Cone(){};

    /**
     * @brief Create a new Cone by parsing the string description parameter
     *
     * @param description - The description to be parsed
     * @return The Cone created by parsing.
     */
    static Cone *create(string description);

    /**
     * @brief Get the Cone position
     *
     * @return The Vector corresponding to the position
     */
    virtual Vector getPosition();

    /**
      * @brief Check if the ray intersect the Cone and save it in the intersection parameter if true
      *
      * @param ray - the ray
      * @param intersection - the intersection
      * @return true if the ray intersect the Cone, else return false
      */
    virtual bool intersect(const Ray &ray, Intersection &intersection);

    virtual ShapeTypes getShape();


    /**
     * @brief Construct the material information in string format
     * @return the string created
     */
    virtual operator std::string() const;
};

#endif //RAYTRACER_CONE_H
