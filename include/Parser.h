/**
 * @file Parser.h
 * @author Florent Simonnot & Violaine Huynh & Jean-Baptiste Pilorget
 * @brief Parser
 * @version 1.0
 * @date 2020-06-06
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef RAYTRACER_PARSER_H
#define RAYTRACER_PARSER_H

#include "Scene.h"
#include "Cube.h"
#include "Cone.h"
#include "Cylinder.h"
#include "Triangle.h"
#include "Material.h"
#include "Plane.h"
#include "ShapeTypes.h"
#include <string.h>

/**
 * @class Parser
 *
 * @brief Manage a Parser.
 *
**/
class Parser
{
private:
    int level;
    int rays;
    string input;
    string output;
    bool debugMode;

    /**
     * @brief Construct a new Parser with specific parameters
     *
     * @param lev - parser level
     * @param in - parser input
     * @param out - parser output
     * @param r - parser rays
     * @param debugMode - flag if user want use the debugger
     */
    Parser(int lev, string in, string out, int r, bool debugMode) : level(lev), rays(r), input(in), output(out), debugMode(debugMode){};

    /**
     * @brief Initialize a Parser with custom properties and options
     *
     * @param argc - number of arguments
     * @param argv - the arguments
     * @param opts - the options
     * @return the parser initialized
     */
    static Parser init(int argc, char **argv, string opts);

    /**
     * @brief Add the proper object to the scene depending on the shape
     *
     * @param scene - the scene
     * @param type - the shape type
     * @param description - the description
     */
    static void addToScene(Scene &scene, ShapeTypes type, string description);

public:
    /**
     * @brief Default Parser constructor
     */
    ~Parser(){};

    /**
     * @brief Create a scene with the properties given in arguments and the shape
     * obtained by parsing the file given
     *
     * @param argc - number of arguments
     * @param argv - the arguments
     * @return the scene created
     */
    static Scene parser(int argc, char **argv);
};

#endif //RAYTRACER_PARSER_H
