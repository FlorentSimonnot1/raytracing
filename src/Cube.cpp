#include "../include/Cube.h"
#include "Tools.cpp"
#include <iostream>
using namespace std;

Cube *Cube::create(string description)
{
    int nbPoints = 4;
    stringstream iss(description);
    Vector vectors[nbPoints];
    for (int i = 0; i < nbPoints; i++)
    {
        vectors[i] = Tools::parseVector(iss, ',');
    }
    Vector color = Tools::parseVector(iss, ',');
    if (iss.get() == -1)
    {
        return new Cube(vectors, color);
    }
    string m = Tools::parseName(iss, ',');
    return new Cube(vectors, color, m);
}

double Cube::getMin(const char var)
{
    double minVector;
    int index;
    switch (var)
    {
    case 'x':
        index = 0;
        minVector = min(min(p1[index], p2[index]), min(p3[index], p4[index]));
        if (p1[index] < p2[index] && p1[index] < p3[index] && p1[index] < p4[index])
        {
            return p1[index];
        }
        break;
    case 'y':
        index = 1;
        minVector = min(min(p1[index], p2[index]), min(p3[index], p4[index]));
        if (p1[index] < p2[index] && p1[index] < p3[index] && p1[index] < p4[index])
        {
            return p1[index];
        }
        break;
    case 'z':
        index = 2;
        minVector = min(min(p1[index], p2[index]), min(p3[index], p4[index]));
        if (p1[index] < p2[index] && p1[index] < p3[index] && p1[index] < p4[index])
        {
            return p1[index];
        }
        break;
    default:
        exit(1);
    }
    return p1[index] - abs(minVector);
}

double Cube::getMax(const char var)
{
    double maxVector;
    int index;
    switch (var)
    {
    case 'x':
        index = 0;
        maxVector = max(max(p1[index], p2[index]), max(p3[index], p4[index]));
        if (p1[index] > p2[index] && p1[index] > p3[index] && p1[index] > p4[index])
        {
            return p1[index];
        }
        break;
    case 'y':
        index = 1;
        maxVector = max(max(p1[index], p2[index]), max(p3[index], p4[index]));
        if (p1[index] > p2[index] && p1[index] > p3[index] && p1[index] > p4[index])
        {
            return p1[index];
        }
        break;
    case 'z':
        index = 2;
        maxVector = max(max(p1[index], p2[index]), max(p3[index], p4[index]));
        if (p1[index] > p2[index] && p1[index] > p3[index] && p1[index] > p4[index])
        {
            return p1[index];
        }
        break;
    default:
        exit(1);
    }
    return p1[index] + abs(maxVector);
}

ShapeTypes Cube::getShape(){
    return ShapeTypes::CUBE;
}

bool Cube::intersect(const Ray &ray, Intersection &intersection)
{
    Vector inverseVector = ray.direction.getInverse();

    int size = 3;
    char letters[3] = {
        'x',
        'y',
        'z'};
    double minXYZ[size];
    double maxXYZ[size];

    for (int i = 0; i < size; i++)
    {
        double r1 = (getMin(letters[i]) - ray.origin[i]) * inverseVector[i];
        double r2 = (getMax(letters[i]) - ray.origin[i]) * inverseVector[i];
        minXYZ[i] = std::min(r1, r2);
        maxXYZ[i] = std::max(r1, r2);
    }

    double last = std::min(std::min(maxXYZ[0], maxXYZ[1]), maxXYZ[2]);
    if (last < 0)
        return false;

    double first = std::max(std::max(minXYZ[0], minXYZ[1]), minXYZ[2]);

    if (last - first >= -0.000001)
    {
        intersection.t = first > 0 ? first : 0;
        intersection.pos = ray.origin + ray.direction * intersection.t;
        Vector pos = p1;
        intersection.normal = (intersection.pos - pos).getNormalized();
        intersection.shape = &(*this);

        return true;
    }

    return false;
}

Vector Cube::getPosition()
{
    return p1;
}

Cube::operator std::string() const
{
    stringstream ss;
    ss << "Cube { point : " << p1 << ", u : " << p2 << ", v : " << p3 << ", w: " << p4 << ", color: " << color << ", material: " << materialName << " }";
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, Cube cube)
{
    string s = cube;
    os << s;
    return os;
}