#include <cmath>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include "../include/Material.h"
#include "../include/Vector.h"

using namespace std;
class Material;
class Vector;
class Tools
{
public:
    static constexpr int MAXDEPTH = 10;
    static constexpr double EPSILON = 0.00000001;

    static void writeDebugInfo(string info)
    {
        std::cout << "\033[36m" << info << "\033[0m" << std::endl;
    }

    static void writeErrorInfo(string info)
    {
        std::cerr << "\033[31m" << info << "\033[0m" << std::endl;
    }

    static void checkFileExtension(string file, string extension)
    {
        if (file.substr(file.find_last_of(".") + 1) != extension)
        {
            Tools::writeErrorInfo("File should be a ." + extension + " file !");
            exit(EXIT_FAILURE);
        }
    }

    static double gammaCorrection(const double composant)
    {
        return pow(composant, 1 / 2.2);
    }

    static double clamp(const double composant, double minValue, double maxValue)
    {
        return std::min(maxValue, std::max(minValue, composant));
    }

    static size_t occurence(const string &s, const string &reg, int n)
    {
        size_t pos = 0;
        int count = 0;
        while (count != n)
        {
            pos++;
            pos = s.find(reg, pos);
            if (pos == std::string::npos)
            {
                return -1;
            }
            count++;
        }
        return pos;
    }

    static Vector parseVector(std::stringstream &ss, const char &sep)
    {
        string line;
        getline(ss, line, sep);
        stringstream ssVector(line);
        Vector v;
        ssVector >> v;
        return v;
    }

    static Vector parseColor(std::stringstream &ss, const char &sep)
    {
        string line;
        getline(ss, line, sep);
        stringstream ssVector(line);
        Vector color;
        ssVector >> color;
        return color / 255;
    }

    static string parseName(std::stringstream &ss, const char &sep)
    {
        string line;
        string name;
        getline(ss, line, sep);
        stringstream ssName(line);
        ssName >> name;
        return name;
    }

    static double parseDouble(std::stringstream &ss, const char &sep)
    {
        string line;
        double res;
        getline(ss, line, sep);
        stringstream ssValue(line);
        ssValue >> res;
        return res;
    }

    static bool parseBoolean(std::stringstream &ss, const char &sep)
    {
        string line;
        bool value;
        getline(ss, line, sep);
        stringstream ssBool(line);
        ssBool >> std::boolalpha >> value;
        return value;
    }

    static Material parseMaterial(const string &s, std::stringstream &iss, const char &sep, int n)
    {
        /*""
        "
            Parse the stream in a new Material
            ""
            "*/
        string line;
        stringstream ss;
        ss << sep;
        size_t position = Tools::occurence(s, ss.str(), n);
        Material m;
        if (position == -1)
        {
            return m;
        }
        getline(iss, line, sep);
        stringstream issMaterial(line);
        issMaterial >> m;
        return m;
    }
};