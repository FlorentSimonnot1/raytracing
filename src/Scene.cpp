#include "../include/Scene.h"
#include "Tools.cpp"
#include <cmath>

using namespace std;

void Scene::checkScene()
{
    if (input.length() == 0)
    {
        Tools::writeErrorInfo("Input file is incorrect");
        exit(EXIT_FAILURE);
    }
    if (level != 2)
    {
        if (output.length() == 0)
        {
            Tools::writeErrorInfo("Ouput file is incorrect");
            exit(EXIT_FAILURE);
        }
    }
    Tools::checkFileExtension(input, "txt");
    //Tools::checkFileExtension(output, "ppm");
}

void Scene::addCamera(const Camera &c)
{
    camera = c;
}

void Scene::addLight(const Light &l)
{
    lights.push_back(l);
}

void Scene::addImage(const Image &img)
{
    image = img;
}

void Scene::addMaterial(Material material)
{
    materials[material.getName()] = material;
}

void Scene::addShape(const ShapeTypes &shapeType, Shape *shape)
{
    shapes[shapeType]
        .push_back(shape);
}

bool Scene::intersect(const Ray &ray, Intersection &intersection) const
{
    bool hasIntersect = false;
    intersection.t = INFINITY;
    for (auto categorie = shapes.begin(); categorie != shapes.end(); categorie++)
    {
        for (auto shape = categorie->second.begin(); shape != categorie->second.end(); shape++)
        {
            Intersection local;
            bool localIntersect = (**shape).intersect(ray, local);
            if (localIntersect)
            {
                hasIntersect = true;
                if (local.t < intersection.t)
                {
                    intersection = local;
                }
            }
        }
    }
    return hasIntersect;
}

bool Scene::intersectShadow(Intersection &intersection) const
{
    for (auto light = lights.begin(); light != lights.end(); light++)
    {
        Vector v = light->getPos() - intersection.pos;
        double dLight2 = dot(v, v);
        Ray rayLight = Ray(intersection.pos + intersection.normal * 0.01, v.getNormalized());
        for (auto categorie = shapes.begin(); categorie != shapes.end(); categorie++)
        {
            for (auto shape = categorie->second.begin(); shape != categorie->second.end(); shape++)
            {
                Intersection local;
                bool localIntersect = (**shape).intersect(rayLight, local);
                if (localIntersect && local.t * local.t <= dLight2)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

void Scene::generateScene()
{
#pragma omp parallel for
    for (int i = 0; i < image.getHeight(); i++)
    {

        for (int j = 0; j < image.getWidth(); j++)
        {

            Ray ray = camera.createRay(i, j, image);
            Vector color;
            int nrays = level == 3 ? rays : 1;

            for (int k = 0; k < nrays; k++)
            {
                color += getColor(ray, 5) / nrays;
            }

            Image::Pixel p = Image::Pixel(
                Tools::clamp(Tools::gammaCorrection(color[0]), 0., 255.),
                Tools::clamp(Tools::gammaCorrection(color[1]), 0., 255.),
                Tools::clamp(Tools::gammaCorrection(color[2]), 0., 255.));
            image.setPixel(i, j, p);
        }
    }
}

void Scene::getMirrorColor(Vector &color, const Ray &ray, const Intersection &intersection, int &nRecursion)
{
    Vector normal = intersection.normal;
    Vector mirrorDirection = ray.direction - normal * 2 * dot(normal, ray.direction);
    Ray mirrorRay(intersection.pos + normal * 0.001, mirrorDirection);
    color = getColor(mirrorRay, nRecursion--);
}

void Scene::getTransparencyColor(Vector &color, const Ray &ray, const Intersection &intersection, int &nRecursion, double refractionCoef)
{
    Vector normal = intersection.normal;
    double coef = refractionCoef;
    Vector normalT = intersection.normal;
    if (dot(normal, ray.direction) > 0)
    {
        coef = 1 / coef;
        normalT = normal * (-1);
    }
    double radical = 1 - pow(coef, 2) * (1 - pow(dot(normalT, ray.direction), 2));
    if (radical > 0)
    {
        Vector directionRefraction = (ray.direction - normalT * dot(normalT, ray.direction)) * (coef)-normalT * sqrt(radical);
        Ray rayRefraction = Ray(intersection.pos - normalT * 0.001, directionRefraction);
        color = getColor(rayRefraction, nRecursion--);
    }
}

Material Scene::searchMaterialByName(const string name) const
{
    // Get the shape material
    Material mat = Material();
    for (auto it = materials.begin(); it != materials.end(); ++it)
    {
        if (it->first == name)
        {
            mat = it->second;
        }
    }

    return mat;
}

Vector Scene::getColor(const Ray &ray, int nRecursion)
{
    if (nRecursion <= 0)
    {
        return Vector(0, 0, 0);
    }

    Intersection intersection, lightIntersection;
    bool hasIntersect = intersect(ray, intersection);

    Vector pixelIntensity = Vector(0, 0, 0);
    if (hasIntersect)
    {

        string materialName = intersection.shape->getMaterialName();
        Material mat = searchMaterialByName(materialName);

        //Mirror object
        if (mat.getReflectionCoef() > 0 && intersection.shape->getShape() != ShapeTypes::CUBE)
        {
            getMirrorColor(pixelIntensity, ray, intersection, nRecursion);
        }
        else
        {
            //Transparent object
            if (mat.getRefractionCoef() > 0)
            {
                getTransparencyColor(pixelIntensity, ray, intersection, nRecursion, mat.getRefractionCoef());
            }
            // Diffuse object
            else
            {
                if (level == 3)
                {
                    bool shadow = intersectShadow(intersection);
                    if (shadow == false)
                    {
                        for (auto light = lights.begin(); light != lights.end(); light++)
                        {
                            pixelIntensity += light->getColor(*this, ray, intersection);
                        }
                    }

                    if (rays > 0)
                    {
                        Vector random = randomCos(intersection.normal);
                        Ray randomRay = Ray(intersection.pos + intersection.normal * 0.001, random);
                        pixelIntensity += intersection.shape->getColor() * getColor(randomRay, nRecursion - 1);
                    }
                }
                else
                {
                    //Lightning with Blinn Phong
                    for (auto light = lights.begin(); light != lights.end(); light++)
                    {
                        pixelIntensity += light->getColor((*this), ray, intersection);
                    }
                }
            }
        }
    }

    return pixelIntensity;
}

void Scene::generateImage(const string &fileName)
{
    if (getFileExtension() == "ppm")
    {
        if (isDebugMode)
            Tools::writeDebugInfo("Write result in PPM file");
        image.savePPM(fileName);
    }
    else
    {
        Tools::writeDebugInfo("Write result in BMP file");
        image.saveBMP(fileName.c_str());
    }
}

Scene::operator std::string() const
{
    stringstream ss;
    ss << "Lights : " << endl;
    for (auto l = lights.begin(); l != lights.end(); l++)
    {
        ss << "\t" << *l << endl;
    }
    ss << "Materials : " << endl;
    for (auto i = materials.begin(); i != materials.end(); i++)
    {
        ss << "\t" << i->first << " : " << i->second << endl;
    }
    ss << "Shapes : " << endl;
    for (auto i = shapes.begin(); i != shapes.end(); i++)
    {
        for (auto s = i->second.begin(); s != i->second.end(); s++)
        {
            ss << "\t" << (**s) << endl;
        }
    }
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, Scene &scene)
{
    string s = scene;
    os << s;
    return os;
}
