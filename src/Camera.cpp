#include "../include/Camera.h"
#include "Tools.cpp"

using namespace std;

Camera::Camera(Vector lookfrom, Vector lookat, Vector vup, double vfov, double aspectRatio, double aperture, double dist)
    : pos(lookfrom), lookat(lookat), vup(vup), vfov(vfov), aspectRatio(aspectRatio), aperture(aperture), dist(dist)
{

    auto theta = vfov * M_PI / 180;
    auto h = tan(theta / 2);
    auto viewport_height = 2.0 * h;
    auto viewport_width = aspectRatio * viewport_height;

    w = (lookfrom - lookat).getNormalized();
    u = cross(vup, w).getNormalized();
    v = cross(w, u);

    horizontal = dist * viewport_width * u;
    vertical = dist * viewport_height * v;
    lowerLeftCorner = pos - horizontal / 2 - vertical / 2 - dist * w;
    lensRadius = aperture / 2;
}

Camera *Camera::create(string description)
{

    stringstream iss(description);
    char seperator = ',';
    Vector lookfrom = Tools::parseVector(iss, seperator);
    Vector lookat = Tools::parseVector(iss, seperator);
    Vector vup = Tools::parseVector(iss, seperator);
    double vfov = Tools::parseDouble(iss, seperator);
    double aspectRatio = Tools::parseDouble(iss, seperator);
    double aperture = Tools::parseDouble(iss, seperator);
    double dist = Tools::parseDouble(iss, seperator);
    return new Camera(lookfrom, lookat, vup, vfov, aspectRatio, aperture, dist);
}

const Vector Camera::getPos() const
{
    return pos;
}

double random_double()
{
    return rand() / (RAND_MAX + 1.0);
}

double random_double(double min, double max)
{
    return min + (max - min) * random_double();
}

Vector random_in_unit_disk()
{
    while (true)
    {
        auto p = Vector(random_double(-1, 1), random_double(-1, 1), 0);
        if (dot(p, p) >= 1)
            continue;
        return p;
    }
}

Ray Camera::createRay(const int &i, const int &j, Image &im)
{
    double s = (j + random_double()) / (im.getHeight() - 1), t = (i + random_double()) / (im.getWidth() - 1);
    Vector rd = lensRadius * random_in_unit_disk();
    Vector offset = u * rd[0] + v * rd[1];
    Vector dir = lowerLeftCorner + s * horizontal + t * vertical - pos - offset;
    return Ray(pos + offset, dir.getNormalized());
}

Camera &Camera::setLookAt(const double x, const double y, const double z)
{
    return *this = Camera(pos, lookat + Vector(x, y, z), vup, vfov, aspectRatio, aperture, dist);
}

Camera &Camera::setPosition(const double x, const double y, const double z)
{
    return *this = Camera(pos + Vector(x, y, z), lookat, vup, vfov, aspectRatio, aperture, dist);
}

Camera &Camera::setVfov(double v)
{
    return *this = Camera(pos, lookat, vup, vfov + v, aspectRatio, aperture, dist);
}

Camera::operator std::string() const
{
    stringstream ss;
    ss << "Camera (position:" << pos
       << ", lower left corner:" << lowerLeftCorner
       << ", horizontal:" << horizontal
       << ", vertical:" << vertical
       << ", u:" << u
       << ", v:" << v
       << ", w:" << w
       << ", lens ratio:" << lensRadius
       << "front of view:" << vfov
       << ")";
    return ss.str();
}

void Camera::move(Camera::Direction dir)
{
    Vector move(0, 0, 0);
    switch (dir)
    {
    case Camera::Direction::left:
        move = Vector(-10, 0, 0);
        break;
    case Camera::Direction::up:
        move = Vector(0, 10, 0);
        break;
    case Camera::Direction::right:
        move = Vector(10, 0, 0);
        break;
    case Camera::Direction::down:
        move = Vector(0, -10, 0);
        break;
    default:
        break;
    }
    pos += move;
}

void Camera::zoom(Camera::Direction dir)
{
    double addVfov = 0;
    switch (dir)
    {
    case Camera::Direction::forward:
        addVfov -= 10;
        break;
    case Camera::Direction::backward:
        addVfov += 10;
        break;
    default:
        break;
    }
    std::cout << addVfov << std::endl;
    *this = Camera(pos, lookat, vup, vfov + addVfov, aspectRatio, aperture, dist);
}

void Camera::moveLookAt(Camera::Direction dir)
{
    Vector move(0, 0, 0);
    switch (dir)
    {
    case Camera::Direction::left:
        move = Vector(-10, 0, 0);
        break;
    case Camera::Direction::up:
        move = Vector(0, 10, 0);
        break;
    case Camera::Direction::right:
        move = Vector(10, 0, 0);
        break;
    case Camera::Direction::down:
        move = Vector(0, -10, 0);
        break;
    default:
        break;
    }
    *this = Camera(pos, lookat + move, vup, vfov, aspectRatio, aperture, dist);
}

void Camera::setUp(Camera::Direction dir)
{
    Vector move(0, 0, 0);
    switch (dir)
    {
    case Camera::Direction::up:
        move = Vector(1, 0, 0);
        break;
    case Camera::Direction::right:
        move = Vector(0, 1, 0);
        break;
    case Camera::Direction::forward:
        move = Vector(0, 0, 1);
        break;
    }
    *this = Camera(pos, lookat, vup + move, vfov, aspectRatio, aperture, dist);
}

std::ostream &operator<<(std::ostream &os, const Camera &camera)
{
    string text = camera;
    os << text;
    return os;
}