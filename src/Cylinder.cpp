#include "../include/Cylinder.h"
#include "Tools.cpp"
#include <iostream>
using namespace std;

Cylinder *Cylinder::create(string description)
{
    stringstream iss(description);
    char sep = ',';
    Vector center = Tools::parseVector(iss, sep);
    double radius = Tools::parseDouble(iss, sep);
    double height = Tools::parseDouble(iss, sep);
    Vector color = Tools::parseVector(iss, sep);
    //No more properties
    if (iss.get() == -1)
    {
        return new Cylinder(center, radius, height, color);
    }
    string materialName = Tools::parseName(iss, ',');
    return new Cylinder(center, radius, height, color, materialName);
}

Vector Cylinder::normalIn(const Vector &vec)
{
    if (vec[0] < basePoint[0] + radius &&
        vec[0] > basePoint[0] - radius &&
        vec[2] < basePoint[2] + radius &&
        vec[2] > basePoint[2] - radius)
    {
        double ep = 0.0000001;
        if (vec[1] < basePoint[1] + height + ep && vec[1] > basePoint[1] + height - ep)
        {
            return Vector(0, 1, 0);
        }
        if (vec[1] < basePoint[1] + ep && vec[1] > basePoint[1] - ep)
        {
            return Vector(0, -1, 0);
        }
    }

    Vector c(basePoint[0], vec[1], basePoint[2]);
    Vector v = vec - c;
    v.normalize();
    return v;
}

bool Cylinder::baseIntersect(const Ray &ray, const Vector &c, Intersection &intersection)
{
    Vector normal = normalIn(c);
    Vector p(ray.origin[0] - basePoint[0], ray.origin[1] - basePoint[1], ray.origin[2] - basePoint[2]);
    double px = normal[0], py = normal[1], pz = normal[2];
    double d = -(px * (c[0] - basePoint[0]) + py * (c[1] - basePoint[1]) + pz * (c[2] - basePoint[2]));

    if (px * ray.direction[0] + py * ray.direction[1] + pz * ray.direction[2] == 0)
    {
        return false;
    }

    double dist = -(px * p[0] + py * p[1] + pz * p[2] + d) / (px * ray.direction[0] + py * ray.direction[1] + pz * ray.direction[2]);

    double ep = 0.00000001;
    if (dist < ep)
    {
        return false;
    }

    Vector pp(p[0] + dist * ray.direction[0], p[1] + dist * ray.direction[1], p[2] + dist * ray.direction[2]);
    if (pp[0] * pp[0] + pp[2] * pp[2] - radius * radius > ep)
    {
        return false;
    }

    intersection.t = dist;
    intersection.pos = ray.origin + intersection.t * ray.direction;
    intersection.normal = normal;
    intersection.shape = this;
    return true;
}

Vector Cylinder::getPosition()
{
    return topPoint;
}

ShapeTypes Cylinder::getShape(){
    return ShapeTypes::CYLINDER;
}

bool Cylinder::intersect(const Ray &ray, Intersection &intersection)
{
    Vector origin(ray.origin);
    Vector direction(ray.direction);
    Vector p(origin[0] - basePoint[0], origin[1] - basePoint[1], origin[2] - basePoint[2]);

    double a = direction[0] * direction[0] + direction[2] * direction[2];
    double b = direction[0] * p[0] + direction[2] * p[2];
    double c = p[0] * p[0] + p[2] * p[2] - radius * radius;
    double delta = b * b - a * c;
    double ep = 0.00000001;

    if (delta < ep)
    {
        return false;
    }

    double t = (-b - sqrt(delta)) / a;
    if (t <= ep)
    {
        return false;
    }

    double y = p[1] + t * direction[1];

    if (y > height + ep || y < -ep)
    {
        Intersection i;
        bool bi = baseIntersect(ray, topPoint, i);
        if (bi)
            t = i.t;
        bool bj = baseIntersect(ray, basePoint, i);
        if (bj && i.t > ep && t >= i.t)
        {
            t = i.t;
        }
        i.t = t;
        intersection = i;
        return bi || bj;
    }

    intersection.t = t;
    intersection.pos = origin + t * direction;
    intersection.normal = normalIn(intersection.pos);
    intersection.shape = this;
    return true;
}