#include <iostream>
#include "math.h"
#include "../include/Sphere.h"
#include "Tools.cpp"

std::ostream &operator<<(std::ostream &strm, const Sphere &s)
{
    string text = s;
    strm << text;
    return strm;
}

Sphere::operator std::string() const
{
    stringstream strm;
    strm << "Sphere { position: " << origin << ", radius : " << radius << ", color " << color << ", material " << materialName << " }";
    return strm.str();
}

ShapeTypes Sphere::getShape(){
    return ShapeTypes::SPHERE;
}

bool Sphere::intersect(const Ray &ray, Intersection &intersection)
{
    double a = 1;
    double b = 2 * dot(ray.direction, ray.origin - origin);
    double c = (ray.origin - origin).getNorm2() - radius * radius;
    double delta = b * b - 4 * a * c;

    if (delta < 0)
        return false;

    double t1 = (-b - sqrt(delta)) / (2 * a);
    double t2 = (-b + sqrt(delta)) / (2 * a);

    if (t2 < 0)
        return false;

    if (t1 > 0)
        intersection.t = t1;
    else
        intersection.t = t2;

    intersection.pos = ray.origin + ray.direction * intersection.t;
    intersection.normal = (intersection.pos - origin).getNormalized();
    intersection.shape = &(*this);
    return true;
}

Vector Sphere::getPosition()
{
    return origin;
}

Sphere *Sphere::create(string description)
{
    stringstream iss(description);
    Vector pos = Tools::parseVector(iss, ',');
    double radius = Tools::parseDouble(iss, ',');
    Vector color = Tools::parseVector(iss, ',');
    //No more properties
    if (iss.get() == -1)
    {
        return new Sphere(pos, radius, color);
    }
    string materialName = Tools::parseName(iss, ',');
    return new Sphere(pos, radius, color, materialName);
}