#include "../include/Shape.h"

#include <sstream>

const Vector Shape::getColor() const
{
    return color;
}

const string Shape::getMaterialName() const
{
    return materialName;
}

Shape::operator std::string() const
{
    stringstream ss;
    ss << "shape";
    return ss.str();
}