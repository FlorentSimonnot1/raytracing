//
// Created by FlorentSimonnot on 21/04/2020.

#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include "../include/Vector.h"
#include "Tools.cpp"
#include "../include/Image.h"
using namespace std;

Image &Image::operator=(const Image &image)
{
    this->width = image.width;
    this->height = image.height;
    if (this->pixels != NULL)
    {
        delete[](this->pixels);
    }
    this->pixels = new Pixel[this->width * this->height];
    for (int i = 0; i < this->width * this->height; i++)
    {
        this->pixels[i] = image[i];
    }
    return *this;
}

Image *Image::create(string description)
{
    string line;
    double w, h;
    Vector color = Vector();
    stringstream iss(description);
    getline(iss, line, ',');
    stringstream issSize(line);
    issSize >> w >> h;
    if (Tools::occurence(description, ",", 1) == -1)
    {
        return new Image(w, h);
    }
    return new Image(w, h);
}

void Image::savePPM(const string &filename)
{
    if (getWidth() == 0 || getHeight() == 0)
    {
        fprintf(stderr, "Can't save an empty image\n");
        return;
    }
    std::ofstream ofs;
    try
    {
        ofs.open(filename, std::ios::binary);
        if (ofs.fail())
            throw("Can't open output file");
        ofs << "P6\n"
            << getWidth() << " " << getHeight() << "\n255\n";
        unsigned char r, g, b;
        // loop over each pixel in the image, clamp and convert to byte format
        for (int i = 0; i < getWidth() * getHeight(); ++i)
        {

            r = this->pixels[i].red;
            g = this->pixels[i].green;
            b = this->pixels[i].blue;
            ofs << r << g << b;
        }
        ofs.close();
    }
    catch (const char *err)
    {
        fprintf(stderr, "%s\n", err);
        ofs.close();
    }
}

const int *Image::toIntArray() const
{
    int *res = new int[width * height * sizeof(int)];
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            res[j + i * width] = pixels[j + i * width].red * 0x10000 + pixels[j + i * width].blue * 0x100 + pixels[j + i * width].green;
        }
    }
    return res;
}

void Image::saveBMP(const char *filename)
{
    if (width == 0 || height == 0)
    {
        fprintf(stderr, "Can't save an empty image\n");
        return;
    }
    unsigned char bmpFileHeader[14] = {'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0};
    unsigned char bmpInfoHeader[40] = {40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0};
    unsigned char bmpPad[3] = {0, 0, 0};

    int fileSize = 54 + 3 * width * height;
    bmpFileHeader[2] = (unsigned char)(fileSize);
    bmpFileHeader[3] = (unsigned char)(fileSize >> 8);
    bmpFileHeader[4] = (unsigned char)(fileSize >> 16);
    bmpFileHeader[5] = (unsigned char)(fileSize >> 24);

    bmpInfoHeader[4] = (unsigned char)(width);
    bmpInfoHeader[5] = (unsigned char)(width >> 8);
    bmpInfoHeader[6] = (unsigned char)(width >> 16);
    bmpInfoHeader[7] = (unsigned char)(width >> 24);
    bmpInfoHeader[8] = (unsigned char)(height);
    bmpInfoHeader[9] = (unsigned char)(height >> 8);
    bmpInfoHeader[10] = (unsigned char)(height >> 16);
    bmpInfoHeader[11] = (unsigned char)(height >> 24);

    FILE *f;
    f = fopen(filename, "wb");
    if (f != nullptr)
    {
        fwrite(bmpFileHeader, 1, 14, f);
        fwrite(bmpInfoHeader, 1, 40, f);
        std::vector<unsigned char> bgrPixels(width * height * 3);
        for (int i = 0; i < height * width; i++)
        {
            bgrPixels[i * 3] = pixels[i].blue;
            bgrPixels[i * 3 + 1] = pixels[i].green;
            bgrPixels[i * 3 + 2] = pixels[i].red;
        }

        for (int i = 0; i < height; i++)
        {
            fwrite(&bgrPixels[0] + (width * (height - i - 1) * 3), 3, width, f);
            fwrite(bmpPad, 1, (4 - (width * 3) % 4) % 4, f);
        }
        fclose(f);
    }
}