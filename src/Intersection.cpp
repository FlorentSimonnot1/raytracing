#include "../include/Intersection.h"

Intersection::~Intersection() {}

Intersection &Intersection::operator=(const Intersection &i)
{
    this->pos = i.pos;
    this->normal = i.normal;
    this->shape = i.shape;
    this->t = i.t;
    return *this;
}
