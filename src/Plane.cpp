#include "../include/Plane.h"
#include "Tools.cpp"
#include <sstream>

Plane::Plane(Vector pos, Vector normal, Vector color, string materialName) : pos(pos), normal(normal)
{
    this->color = color;
    this->materialName = materialName;
}

Plane::Plane() : Plane(Vector(), Vector(), Vector()) {}

Plane::~Plane() {}

Plane *Plane::create(string description)
{
    stringstream iss(description);
    Vector pos = Tools::parseVector(iss, ',');
    Vector normal = Tools::parseVector(iss, ',');
    Vector color = Tools::parseVector(iss, ',');
    if (iss.get() == -1)
    {
        return new Plane(pos, normal, color);
    }
    string materialName = Tools::parseName(iss, ',');
    return new Plane(pos, normal, color, materialName);
}

ShapeTypes Plane::getShape(){
    return ShapeTypes::PLANE;
}

bool Plane::intersect(const Ray &ray, Intersection &intersection)
{
    double dDotN = dot(ray.direction, normal);
    if (dDotN == 0.)
    {
        return false;
    }

    double t = dot((pos - ray.origin), normal) / dDotN;
    if (t < 0.00001)
    {
        return false;
    }
    intersection.t = t;
    intersection.shape = this;
    intersection.pos = ray.origin + ray.direction * intersection.t;
    intersection.normal = normal.getNormalized();
    return true;
}

Plane::operator string() const
{
    stringstream ss;
    ss << "Plane { position: " << pos << ", normal: " << normal << ", color " << color << ", material " << materialName << "}";
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, const Plane &plane)
{
    string text = plane;
    os << text;
    return os;
}