#include "../include/Parser.h"
#include <getopt.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <ctype.h>
#include <iostream>
#include "Tools.cpp"

Parser Parser::init(int argc, char **argv, string opts)
{
    int opt;
    int ps = 0;
    int option_index = 0;
    int level = 0;
    bool isDebugMode = false;
    string input;
    string output;

    static struct option long_options[] = {
        {"ps", required_argument, 0, 0},
        {0, 0, 0, 0}};

    while ((opt = getopt_long_only(argc, argv, opts.c_str(), long_options, &option_index)) != EOF)
    {
        switch (opt)
        {
        case 0:
            if (long_options[option_index].name == "ps")
            {
                ps = atoi(optarg);
                if (ps < 1)
                {
                    Tools::writeErrorInfo("Rays number should be gretter than 0");
                    exit(EXIT_FAILURE);
                }
            }
            break;
        case 'n':
            level = atoi(optarg);
            if (level < 1 || level > 3)
            {
                Tools::writeErrorInfo("Level should be 1 | 2 | 3");
                exit(EXIT_FAILURE);
            }
            break;
        case 'i':
            input = optarg;
            break;
        case 'o':
            output = optarg;
            break;
        case 'd':
            isDebugMode = true;
            Tools::writeDebugInfo("[INFO] Debug mode activate");
            break;
        default:
            break;
        }
    }
    return Parser(level, input, output, ps, isDebugMode);
}

void Parser::addToScene(Scene &scene, ShapeTypes shape, string description)
{
    switch (shape)
    {
    case IMAGE:
        scene.addImage((*Image::create(description)));
        break;
    case CAMERA:
        scene.addCamera((*Camera::create(description)));
        break;
    case LIGHT:
        scene.addLight((*Light::create(description)));
        break;
    case SPHERE:
        scene.addShape(SPHERE, Sphere::create(description));
        break;
    case CUBE:
        scene.addShape(CUBE, Cube::create(description));
        break;
    case CONE:
        scene.addShape(CONE, Cone::create(description));
        break;
    case CYLINDER:
        scene.addShape(CYLINDER, Cylinder::create(description));
        break;
    case TRIANGLE:
        scene.addShape(TRIANGLE, Triangle::create(description));
        break;
    case PLANE:
        scene.addShape(PLANE, Plane::create(description));
        break;
    case MATERIAL:
        scene.addMaterial(Material::create(description));
        break;
    case NONE:
        break;
    default:
        break;
    }
}

Scene Parser::parser(int argc, char **argv)
{
    string line, shape, description;
    Parser parser = Parser::init(argc, argv, "n:i:o:d");
    Scene s = Scene(parser.level, parser.input, parser.output, parser.rays, parser.debugMode);
    ifstream readFile(parser.input.c_str());

    while (getline(readFile, line))
    {
        stringstream iss(line);
        getline(iss, shape, '(');
        getline(iss, description, ')');
        addToScene(s, parseShape(shape), description);
    }
    readFile.close();
    return s;
}