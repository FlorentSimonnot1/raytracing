#include "../include/ShapeTypes.h"
#include <algorithm>
#include <cctype>
#include <map>
using namespace std;

ShapeTypes parseShape(string shape)
{
    static const map<string, ShapeTypes> shapes{
        {"image", IMAGE},
        {"camera", CAMERA},
        {"sphere", SPHERE},
        {"cube", CUBE},
        {"plane", PLANE},
        {"light", LIGHT},
        {"cone", CONE},
        {"cylinder", CYLINDER},
        {"material", MATERIAL},
        {"triangle", TRIANGLE}};

    shape.erase(remove_if(shape.begin(), shape.end(), ::isspace), shape.end());
    transform(shape.begin(), shape.end(), shape.begin(), ::tolower);
    auto itr = shapes.find(shape);
    if (itr == shapes.end())
    {
        return NONE;
    }
    return itr->second;
}