#include "../include/Light.h"
#include "../include/Vector.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include "Tools.cpp"

using namespace std;

Light *Light::create(string description)
{
    stringstream iss(description);
    Vector position = Tools::parseVector(iss, ',');
    double intensity = Tools::parseDouble(iss, ',');
    //No more properties
    if (iss.get() == -1)
    {
        return new Light(position, intensity);
    }
    double ambient = Tools::parseDouble(iss, ',');
    if (iss.get() == -1)
    {
        return new Light(position, intensity, ambient);
    }
    Vector color = Tools::parseVector(iss, ',');
    return new Light(position, intensity, ambient, color);
}

double Light::getIntensity() const
{
    return intensity;
}

Vector Light::getPos() const
{
    return pos;
}

Vector Light::getColor(const Scene &scene, const Ray &ray, const Intersection &intersection)
{
    string matName = intersection.shape->getMaterialName();
    Material mat = scene.searchMaterialByName(matName);
    Vector ambient = ambientTerm * color;
    Vector normal = intersection.normal;
    Vector lightDirection = (pos - intersection.pos);
    normal.normalize();
    double diffuse = std::max(0.0, dot(normal, lightDirection.getNormalized()));
    Vector diffuseColor = diffuse * mat.getDiffuseProperties().color;

    Vector viewDirection = (scene.getCamera().getLookAt() - pos).getNormalized();
    Vector reflectDirection = (-1 * lightDirection.getNormalized()).reflect(normal);
    double specular = pow(std::max(dot(viewDirection, reflectDirection), 0.0), mat.getSpecularProperties().exponent);
    Vector specularColor = mat.getSpecularProperties().color * specular * color;
    return (ambient + diffuseColor + specularColor) * intersection.shape->getColor() * color * intensity / (4 * M_PI * lightDirection.norm());
}

Light::operator std::string() const
{
    stringstream ss;
    ss << "Light { position : " << pos << ", intensity : " << intensity << ", ambient : " << ambientTerm << ", color: " << color << " }";
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, Light light)
{
    string s = light;
    os << s;
    return os;
}