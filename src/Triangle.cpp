#include "../include/Triangle.h"
#include "Tools.cpp"
#include <sstream>

Triangle *Triangle::create(string description)
{
    stringstream iss(description);
    char separator = ',';
    Vector p1 = Tools::parseVector(iss, separator);
    Vector p2 = Tools::parseVector(iss, separator);
    Vector p3 = Tools::parseVector(iss, separator);
    Vector color = Tools::parseVector(iss, separator);
    if(iss.get() == -1){
        return new Triangle(p1, p2, p3, color);
    }
    string material = Tools::parseName(iss, ',');
    return new Triangle(p1, p2, p3, color, material);
}

Vector Triangle::getPosition()
{
    return p0;
}

ShapeTypes Triangle::getShape(){
    return ShapeTypes::TRIANGLE;
}

bool Triangle::intersect(const Ray &ray, Intersection &intersection)
{
    Vector edge1, edge2, h, g, s, q;
    double a, f, u, v;
    edge1 = p1 - p0;
    edge2 = p2 - p0;
    h = cross(ray.direction, edge2);
    a = dot(edge1, h);
    if (a > -Tools::EPSILON && a < Tools::EPSILON)
    {
        return false;
    }

    f = 1.0 / a;
    s = ray.origin - p0;
    u = f * (dot(s, h));
    if (u < 0.0 || u > 1.0)
    {
        return false;
    }
    q = cross(s, edge1);
    v = f * dot(ray.direction, q);
    if (v < 0.0 || u + v > 1.0)
    {
        return false;
    }
    double t = f * dot(edge2, q);
    if (t > Tools::EPSILON)
    {
        intersection.pos = ray.origin + ray.direction * t;
        intersection.t = t;
        intersection.shape = this;
        intersection.normal = cross(edge2, edge1).getNormalized();
        return true;
    }

    return false;
}