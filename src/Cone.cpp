#include "../include/Cone.h"
#include "Tools.cpp"

Cone::Cone(Vector c, double rad, double h, Vector col, string materialName)
{
    this->center = c;
    this->color = col;
    this->radius = rad;
    this->height = h;
    this->materialName = materialName;
    Vector p1(center[0] + radius, center[1], center[2]);
    Vector p2(center[0], center[1] + height, center[2]);
    double d = (p2 - p1).norm(p2 - p1);
    cosinusAlpha = height / d;
    sinusAlpha = radius / d;
}

Cone *Cone::create(string description)
{
    stringstream iss(description);
    char sep = ',';
    Vector center = Tools::parseVector(iss, sep);
    double radius = Tools::parseDouble(iss, sep);
    double height = Tools::parseDouble(iss, sep);
    Vector color = Tools::parseVector(iss, sep);
    if (iss.get() == -1)
    {
        return new Cone(center, radius, height, color);
    }
    string materialName = Tools::parseName(iss, sep);
    return new Cone(center, radius, height, color, materialName);
}

ShapeTypes Cone::getShape(){
    return ShapeTypes::CONE;
}

Vector Cone::normalIn(const Vector &vec)
{
    Vector u(center[0], vec[1], center[2]);
    Vector v(center[0], center[1] + height, center[2]);
    Vector w = vec - center;
    w = Vector(w[0], 0, w[2]);
    w.normalize();

    Vector n = Vector(w[0] * height / radius, radius / height, w[2] * height / radius);
    return n;
}

bool Cone::baseIntersect(const Ray &ray, const Vector &c, Intersection &intersection)
{
    Vector normal = normalIn(c);
    Vector p0(ray.origin[0] - center[0], ray.origin[1] - center[1], ray.origin[2] - center[2]);
    double aa = normal[0];
    double bb = normal[1];
    double cc = normal[2];
    double dd = (aa * (c[0] - center[0]) + bb * (c[1] - center[1]) + cc * (c[2] - center[2]));

    if (aa * ray.direction[0] + bb * ray.direction[1] + cc * ray.direction[2] == 0)
    {
        return false;
    }

    double dist = -(aa * p0[0] + bb * p0[1] + cc * p0[2] + dd) / (aa * ray.direction[0] + bb * ray.direction[1] + cc * ray.direction[2]);
    double ep = 0.00000001;
    if (dist < ep)
    {
        return false;
    }
    Vector p = Vector(p0[0] + dist * ray.direction[0],
                      p0[1] + dist * ray.direction[1],
                      p0[2] + dist * ray.direction[2]);

    if (p[0] * p[0] + p[2] * p[2] - radius * radius > ep)
    {
        return false;
    }

    intersection.t = dist;
    intersection.pos = ray.origin + intersection.t * ray.direction;
    intersection.normal = normal;
    intersection.shape = this;
    return true;
}

Vector Cone::getPosition()
{
    return center;
}

Cone::operator std::string() const
{
    stringstream strm;
    strm << "Cone { position: " << center << ", radius: " << radius << ", height: " << height << ", color: " << color << ", material: " << materialName << " }";
    return strm.str();
}

bool Cone::intersect(const Ray &ray, Intersection &intersection)
{
    Vector p(ray.origin[0] - center[0], ray.origin[1] - center[1] - height, ray.origin[2] - center[2]);

    double a = cosinusAlpha * ray.direction[0] * ray.direction[0] +
               cosinusAlpha * ray.direction[2] * ray.direction[2] -
               sinusAlpha * ray.direction[1] * ray.direction[1];

    double b = cosinusAlpha * ray.direction[0] * p[0] +
               cosinusAlpha * ray.direction[2] * p[2] -
               sinusAlpha * ray.direction[1] * p[1];

    double c = cosinusAlpha * p[0] * p[0] + cosinusAlpha * p[2] * p[2] - sinusAlpha * p[1] * p[1];

    double delta = b * b - a * c;
    double ep = 0.00000001;
    if (delta < ep)
    {
        return false;
    }
    intersection.t = (-b - sqrt(delta)) / a;
    if (intersection.t < ep)
    {
        return false;
    }

    double y = p[1] + intersection.t * ray.direction[1];
    if (y < -height - ep || y > ep)
    {
        return false;
    }

    intersection.pos = ray.origin + intersection.t * ray.direction;
    intersection.normal = normalIn(intersection.pos);
    intersection.shape = this;
    return true;
}