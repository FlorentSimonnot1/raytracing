#include "../include/Material.h"
#include "Tools.cpp"

#include <sstream>
using namespace std;

Material Material::create(string description)
{
    stringstream iss(description);
    string name = Tools::parseName(iss, ',');
    double reflectionCoef = Tools::parseDouble(iss, ',');
    double refractionIndex = Tools::parseDouble(iss, ',');
    Vector diffuseColor = Tools::parseVector(iss, ',');
    double diffuseCoefficient = Tools::parseDouble(iss, ',');
    Diffuse diffuse(diffuseColor, diffuseCoefficient);
    Vector specularColor = Tools::parseVector(iss, ',');
    double specularExponent = Tools::parseDouble(iss, 'i');
    Specular specular(specularColor, specularExponent);
    Material mat(name, reflectionCoef, refractionIndex, specular, diffuse);
    return mat;
}

Material::operator std::string() const
{
    stringstream ss;
    ss << "Material {name : " << name
       << ", reflectionCoef: " << reflectionCoef << ", refractionIndex: " << refractionIndex
       << ", diffuseColor: " << diffuse.color << ", diffuseCoefficient: " << diffuse.coefficient
       << ", specularColor: " << specular.color << ", specularExponent: " << specular.exponent
       << "}";
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, Material material)
{
    string s = material;
    os << s;
    return os;
}

std::istream &operator>>(std::istream &is, Material &material)
{
    return is >> material.reflectionCoef >> material.refractionIndex;
}