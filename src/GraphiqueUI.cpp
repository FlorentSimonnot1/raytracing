#include "../include/GraphiqueUI.h"
#include "../include/Vector.h"
#include "../include/Image.h"
#include <SDL2/SDL.h>
#include <iostream>

GraphiqueUI::GraphiqueUI(int w, int h) : myWindow(nullptr), myRenderer(nullptr), myTexture(nullptr), width(w), height(h)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cerr << "Error when initialising: " << SDL_GetError() << std::endl;
        return;
    }
    std::cout << height << std::endl;
    myWindow = SDL_CreateWindow("RayTracing", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

    if (myWindow == nullptr)
    {
        std::cerr << "Error when create window: " << SDL_GetError() << std::endl;
    }

    myRenderer = SDL_CreateRenderer(myWindow, -1, SDL_RENDERER_ACCELERATED);
    if (myRenderer == nullptr)
    {
        std::cerr << "Error when create renderer " << SDL_GetError() << std::endl;
    }

    myTexture = SDL_CreateTexture(myRenderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, width, height);
    if (myTexture == nullptr)
    {
        std::cerr << "Could not create texture " << SDL_GetError() << std::endl;
    }

    SDL_SetRenderDrawColor(myRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(myRenderer);
}

GraphiqueUI::GraphiqueUI(const GraphiqueUI &g) : myWindow(g.myWindow), myRenderer(g.myRenderer), myTexture(g.myTexture){};

GraphiqueUI::~GraphiqueUI()
{
    SDL_DestroyTexture(myTexture);
    SDL_DestroyRenderer(myRenderer);
    SDL_DestroyWindow(myWindow);
    SDL_Quit();
}

GraphiqueUI &GraphiqueUI::operator=(const GraphiqueUI &g)
{
    myWindow = g.myWindow;
    myRenderer = g.myRenderer;
    return *this;
}

/*void GraphiqueUI::setPixel(int x, int y, const Vector &color)
{
    SDL_SetRenderDrawColor(myRenderer, color[0], color[1], color[2], SDL_ALPHA_OPAQUE);
    SDL_RenderDrawPoint(myRenderer, x, y);
}*/

void GraphiqueUI::render(const Image *img)
{
    SDL_UpdateTexture(myTexture, NULL, img->toIntArray(), width * sizeof(Uint32));
    SDL_RenderClear(myRenderer);
    SDL_RenderCopy(myRenderer, myTexture, NULL, NULL);
    SDL_RenderPresent(myRenderer);
}