#include "../include/Vector.h"

Vector operator+(const Vector &a, const Vector &b)
{
    return {a[0] + b[0], a[1] + b[1], a[2] + b[2]};
}

Vector &Vector::operator*=(double d)
{
    coords[0] *= d;
    coords[1] *= d;
    coords[2] *= d;
    return *this;
}

Vector &Vector::operator/=(const double d)
{
    coords[0] /= d;
    coords[1] /= d;
    coords[2] /= d;
    return *this;
}

Vector operator-(const Vector &a, const Vector &b)
{
    return {a[0] - b[0], a[1] - b[1], a[2] - b[2]};
}

Vector operator-(const Vector &a)
{
    return {-a[0], -a[1], -a[2]};
}

Vector operator*(const Vector &a, double b)
{
    return {a[0] * b, a[1] * b, a[2] * b};
}

Vector operator*(double a, const Vector &b)
{
    return {b[0] * a, b[1] * a, b[2] * a};
}

Vector operator/(const Vector &a, double b)
{
    return {a[0] / b, a[1] / b, a[2] / b};
}

Vector operator*(const Vector &a, const Vector &b)
{
    return {a[0] * b[0], a[1] * b[1], a[2] * b[2]};
}

double dot(const Vector &a, const Vector &b)
{
    return a[0] *
               b[0] +
           a[1] * b[1] + a[2] * b[2];
}

Vector cross(const Vector &a, const Vector &b)
{
    return {a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0] * b[1] - a[1] * b[0]};
}

Vector Vector::reflect(const Vector &vec)
{
    return *(this) - 2 * dot((*this), vec) * vec;
}

Vector randomCos(const Vector &normal)
{
    double r1 = uniform(engine);
    double r2 = uniform(engine);
    Vector randomDir(cos(2 * M_PI * r1) * sqrt(1 - r2), sin(2 * M_PI * r1) * sqrt(1 - r2), sqrt(r2));
    Vector randomVector(uniform(engine) - 0.5, uniform(engine) - 0.5, uniform(engine) - 0.5);
    Vector tangent1 = cross(normal, randomVector);
    tangent1.normalize();
    Vector tangent2 = cross(tangent1, normal);

    return randomDir[2] * normal + randomDir[0] * tangent1 + randomDir[1] * tangent2;
}

const Vector Vector::getInverse() const
{
    return Vector(1 / coords[0], 1 / coords[1], 1 / coords[2]);
}

double Vector::norm(const Vector &vec)
{
    return sqrt(coords[0] * vec[0] + coords[1] * vec[1] + coords[2] * vec[2]);
}

const double Vector::norm() const
{
    return sqrt(coords[0] * coords[0] + coords[1] * coords[1] + coords[2] * coords[2]);
}

/*************************\
          DISPLAY
\*************************/
std::ostream &
operator<<(std::ostream &strm, const Vector &v)
{
    return strm
           << "V(" << v[0] << ", " << v[1] << ", " << v[2] << ")";
}

std::istream &operator>>(std::istream &is, Vector &v)
{
    return is >>
           v.coords[0] >> v.coords[1] >> v.coords[2];
}