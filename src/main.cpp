#include <iostream>
#include <cmath>
#include "math.h"
#include <random>
#include <thread>
#include <SDL2/SDL.h>
#include <stdio.h>
#include "../include/Vector.h"
#include "../include/Sphere.h"
#include "../include/Image.h"
#include "../include/Scene.h"
#include "../include/Parser.h"
#include "../include/GraphiqueUI.h"
#include "Tools.cpp"

#define WIDTH 1024
#define HEIGHT 1024

int main(int argc, char **argv)
{
    time_t current_time;
    time(&current_time);

    std::cout << "Start process..." << std::endl;

    Scene scene = Parser::parser(argc, argv);
    scene.checkScene();

    if (scene.getDebugMode())
        Tools::writeDebugInfo(scene);

    scene.generateScene();
    Image *image = scene.getImage();

    if (scene.getLevel() == 2)
    {
        GraphiqueUI g(image->getWidth(), image->getHeight());
        bool quit = false;
        SDL_Event event;
        g.render(image);

        while (!quit)
        {
            SDL_WaitEvent(&event);
            if (event.type == SDL_QUIT)
            {
                quit = true;
            }
            if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_UP:
                    scene.moveCameraPosition(Camera::Direction::up);
                    break;
                case SDL_SCANCODE_DOWN:
                    scene.moveCameraPosition(Camera::Direction::down);
                    break;
                case SDL_SCANCODE_LEFT:
                    scene.moveCameraPosition(Camera::Direction::left);
                    break;
                case SDL_SCANCODE_RIGHT:
                    scene.moveCameraPosition(Camera::Direction::right);
                    break;
                case SDL_SCANCODE_P:
                    scene.zoomCamera(Camera::Direction::forward);
                    break;
                case SDL_SCANCODE_SEMICOLON:
                    scene.zoomCamera(Camera::Direction::backward);
                    break;
                case SDL_SCANCODE_I:
                    scene.moveLookAtCamera(Camera::Direction::up);
                    break;
                case SDL_SCANCODE_K:
                    scene.moveLookAtCamera(Camera::Direction::down);
                    break;
                case SDL_SCANCODE_J:
                    scene.moveLookAtCamera(Camera::Direction::left);
                    break;
                case SDL_SCANCODE_L:
                    scene.moveLookAtCamera(Camera::Direction::right);
                    break;
                case SDL_SCANCODE_A:
                    scene.moveUpCamera(Camera::Direction::up);
                    break;
                case SDL_SCANCODE_S:
                    scene.moveUpCamera(Camera::Direction::right);
                    break;
                case SDL_SCANCODE_W:
                    scene.moveUpCamera(Camera::Direction::forward);
                    break;
                default:
                    break;
                }
                scene.generateScene();
                image = scene.getImage();
                g.render(image);
            }
        }
    }
    else
    {
        scene.generateImage(scene.getOutputPath());
    }

    std::cout << "Process Done ! time : " << time(NULL) - current_time << "s" << std::endl;
    return 0;
}
